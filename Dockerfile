FROM python:3.8

RUN pip install paho-mqtt==1.5.0 pyyaml==5.1

ADD MqttHandler.py /home/

ADD main.py /home/

CMD [ "python", "-u", "/home/main.py" ]
