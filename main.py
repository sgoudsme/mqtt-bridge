import os
import asyncio
import time

import yaml

from MqttHandler import MqttHandler

incoming_tpc = "external/incoming/"
outgoing_tpc = "external/outgoing/"

script_path = os.path.dirname(os.path.realpath(__file__))

with open(script_path + '/config.yaml') as file:
    config = yaml.load(file, Loader=yaml.FullLoader)
    int_host = config['internal_host']
    int_port = config['internal_port']
    ext_host = config['external_host']
    ext_port = config['external_port']

internal_mqtt = MqttHandler(int_host, int_port, subList=[f'{outgoing_tpc}#'])
external_mqtt = MqttHandler(ext_host, ext_port, subList=[f'#'])


def int_received(tpc, pld):
    tpc_prefix_len = len(outgoing_tpc)
    external_mqtt.publish(f'{tpc[tpc_prefix_len:]}', pld)


def ext_received(tpc, pld):
    internal_mqtt.publish(f'{incoming_tpc}{tpc}', pld)


internal_mqtt._funct = int_received
external_mqtt._funct = ext_received


async def main():
    while True:
        await asyncio.sleep(100)

asyncio.run(main())

